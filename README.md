# influx_logger microservice




Log data from mqtt to influxdb


## Configuration

On startup the app reads `/app/config.yml` . This file contains overrides for default
configuration defined in `config.Config`

## Development

* use `devcontainer` for development. It will launch docker stack defined in `devcontainer/docker-compose.yml`

It contains:

* devcontainer
* mosquitto broker
* influxdb


### Influxdb

access influx on [localhost:8087](http://localhost:8087).  user: dev , pass: Logger123*

for full configuration see `.devcontainer/influx.env`

## Deployment

Is done by gitlab CI.

1. manually start pipeline to build `sys` image (only needed if image does not exist)
2. run tests in devcontainer
3. bumpversion & push, ci will build the package and application image with tag `app`
