#!/usr/bin/env python3
"""
 Core functionality for influx_logger

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import asyncio
import json
import logging
from datetime import datetime

import aiomqtt as mqtt
import coloredlogs  # type: ignore
from influxdb_client import InfluxDBClient, Point  # type: ignore
from influxdb_client.client.write_api import SYNCHRONOUS  # type: ignore

from influx_logger.config import Config
from influx_logger.utils import flatten_dict

LOG_FORMAT = "%(asctime)s [%(levelname)s] %(filename)s:%(lineno)d - %(message)s"

CFG = Config.from_yaml("/app/config.yml")

log = logging.getLogger(__name__)

point_q: asyncio.Queue = asyncio.Queue()  # points to be written to influxdb


async def write_to_influxdb():
    """write messages from queue to influxdb"""
    client = InfluxDBClient(
        url=CFG.influxdb_url,
        token=CFG.influxdb_token,
        org=CFG.influxdb_org,
    )
    write_api = client.write_api(write_options=SYNCHRONOUS)

    while True:
        point = await point_q.get()
        log.debug("%s", f"writing point: {point}")
        write_api.write(bucket=CFG.influxdb_bucket, record=point)
        point_q.task_done()


async def log_topic(topic: str, measurement: str):
    """log one topic to influxdb"""
    log.info("%s", f"logging topic: {topic} to measurement: {measurement}")

    # just create a client for every topic. This is not very efficient, but
    # avoids filtering messages.
    async with mqtt.Client(CFG.broker_host) as client:
        await client.subscribe(topic)

        async with client.messages() as messages:
            async for message in messages:
                try:
                    # generate timestamp
                    ts_ns = int(datetime.utcnow().timestamp() * 1e9)

                    data = flatten_dict(json.loads(message.payload.decode("utf8")))  # type: ignore

                    log.debug("%s", f"{topic=} {data=}")

                    point = Point(measurement)
                    point.time(ts_ns)
                    for key, value in data.items():
                        point.field(key, value)

                    await point_q.put(point)

                except Exception as err:  # pylint: disable=broad-except
                    log.exception(err)


async def mqtt_client():
    """receive mqtt messages"""

    async with asyncio.TaskGroup() as tg:
        for topic, meas in CFG.topics.items():
            tg.create_task(log_topic(topic, meas))

        tg.create_task(write_to_influxdb())


async def main_loop():
    """main loop"""

    async with asyncio.TaskGroup() as tg:
        tg.create_task(mqtt_client())


def main():
    """main entry point"""

    coloredlogs.install(level="DEBUG", fmt=LOG_FORMAT)

    log.info("Starting influx_logger")

    log.info("%s", f"Loaded config: {CFG}")

    if CFG.debug:
        coloredlogs.set_level("DEBUG")

    # show topics
    log.info("%s", "------------------------Topics:-----------------------")
    for topic, meas in CFG.topics.items():
        log.info("%s", f"topic: {topic} -> measurement: {meas}")

    asyncio.run(main_loop())


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        log.info("Exiting...")
