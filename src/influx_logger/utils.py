#!/usr/bin/env python3
"""
 util functions

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""


def flatten_dict(d, parent_key="", sep="_"):
    items = {}
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.update(flatten_dict(v, new_key, sep=sep))
        elif isinstance(v, list):
            for idx, item in enumerate(v):
                items[f"{new_key}_{idx}"] = item
        else:
            items[new_key] = v
    return items
