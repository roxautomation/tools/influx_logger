#!/usr/bin/env python3
"""
influx_logger CLI
"""

import click
from .version import get_version


@click.group()
def cli():
    pass  # pragma: no cover


@cli.command()
def start():
    """Start influx_logger"""
    from .main import main

    main()


@cli.command()
def info():
    """Print package info"""
    print(get_version())


if __name__ == "__main__":
    cli()  # pragma: no cover
