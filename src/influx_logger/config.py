#!/usr/bin/env python3
"""
 Configuration file for influx_logger

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

from dataclasses import dataclass, field
from typing import Dict
import yaml


def default_topics():
    return {"/test_topic_1": "meas_a", "/test_topic_2": "meas_b"}


@dataclass
class Config:
    """app configuration"""

    broker_host: str = "broker"
    debug: bool = False  # log level

    # influxdb
    influxdb_url: str = "http://influxdb:8086"
    influxdb_org: str = "rox"
    influxdb_bucket: str = "test_bucket"
    influxdb_token: str = "123456"

    # topic to influxdb measurement mapping
    topics: Dict[str, str] = field(default_factory=default_topics)

    def to_yaml(self, fname: str):
        """save config to yaml file"""
        with open(fname, "w", encoding="utf8") as f:
            yaml.dump(self.__dict__, f)

    @classmethod
    def from_yaml(cls, fname: str):
        """load config from yaml file"""
        with open(fname, "r", encoding="utf8") as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            return cls(**data)
