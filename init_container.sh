#!/bin/bash

# This script sets up dev environment inside container

LINE_TO_ADD="export PATH=\$PATH:/home/\$USER/.local/bin"

if ! grep -q "$LINE_TO_ADD" ~/.bashrc; then
    echo "$LINE_TO_ADD" >> ~/.bashrc
fi

source ~/.bashrc


sudo apt update && sudo apt install -y \
    iputils-ping \
    mosquitto-clients \
    tree

pip install -r requirements-dev.txt

# install types
python3 -m pip install types-PyYAML

echo "Running container init, use this script to install libraries etc."
pip install -e .
