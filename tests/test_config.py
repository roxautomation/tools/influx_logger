from influx_logger.config import Config

CFG_FILE = "/tmp/config.yml"


def test_creation():
    cfg = Config()
    assert cfg.broker_host == "broker"


def test_to_yaml():
    cfg = Config()
    cfg.to_yaml(CFG_FILE)


def test_from_yaml():
    cfg = Config.from_yaml(CFG_FILE)
    assert cfg.broker_host == "broker"
    assert cfg.topics == {"/test_topic_1": "meas_a", "/test_topic_2": "meas_b"}
