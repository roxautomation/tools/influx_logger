# type: ignore

"""The setup script."""


import codecs
import os
import os.path

from setuptools import find_packages, setup


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
        else:
            raise RuntimeError("Unable to find version string.")


# please keep this lean and mean. Add dev requirements to .devcontainer/requirments.txt
requirements = ["click", "influxdb-client", "pyyaml", "aiomqtt"]

test_requirements = [
    "pytest>=3",
]

setup(
    author="Rox Automation",
    author_email="dev@roxautomation.com",
    python_requires=">=3.10",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.10",
    ],
    description="Log data from mqtt to influxdb",
    install_requires=requirements,
    include_package_data=True,
    keywords="",
    name="influx_logger",
    package_dir={"": "src"},
    packages=find_packages("src"),
    test_suite="tests",
    tests_require=test_requirements,
    url="",
    version=get_version("src/influx_logger/__init__.py"),
    # zip_safe=False,
    entry_points={"console_scripts": ["influx_logger=influx_logger.cli:cli"]},
)
