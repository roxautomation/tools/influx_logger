#!/bin/bash

docker container prune -f
mkdir -p /var/tmp/container-extensions
docker pull registry.gitlab.com/roxautomation/tools/influx_logger:sys
