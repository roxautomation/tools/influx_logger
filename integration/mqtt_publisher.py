#!/usr/bin/env python3
"""
 publish test data to mqtt broker

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""

import asyncio
import json
import aiomqtt


BROKER_URL = "broker"


async def main():
    async with aiomqtt.Client(BROKER_URL) as client:
        while True:
            for idx in range(10):
                data = {"var_a": -idx, "var_b": 10 - idx}
                await client.publish("/test_topic_1", json.dumps(data))

                data = {"var_c": 20 - idx, "var_d": 30 - idx}
                await client.publish("/test_topic_2", json.dumps(data))

                await asyncio.sleep(1)


if __name__ == "__main__":
    try:
        print("starting mqtt publisher, press Ctrl-C to stop")
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
