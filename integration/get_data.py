#!/usr/bin/env python3
"""
 get dat from influxdb

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""


from influxdb_client import InfluxDBClient

from influx_logger.config import Config

# Load config
cfg = Config.from_yaml("/app/config.yml")


client = InfluxDBClient(url=cfg.influxdb_url, token=cfg.influxdb_token)
query_api = client.query_api()


flux_query = f"""
from(bucket: "{cfg.influxdb_bucket}")
    |> range(start: -1d)
    |> filter(fn: (r) => r["_measurement"] == "meas_b")
    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
    |> keep(columns: ["_time","var_c","var_d"])
"""


print("-------flux query-------")
print(flux_query)

try:
    # Execute the query and fetch the data into a pandas DataFrame
    result = query_api.query_data_frame(org=cfg.influxdb_org, query=flux_query)
    df = result.drop(columns=["result", "table"])
except Exception as e:  # pylint: disable=broad-except
    print(f"error: {e}")
    exit(1)

print(df)
