# Integration tests

This folder contains tools to perform integration tests.

* `mqtt_publisher.py` - continously publish messages to test topics
* `get_data.py` - get today's measurements
